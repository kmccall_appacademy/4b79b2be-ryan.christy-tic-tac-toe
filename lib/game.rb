require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :board

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
  end

  def current_player
    @player_one
  end

  def switch_players!
    @player_one, @player_two = @player_two, @player_one
  end

  #handles the logic for a single turn
  def play_turn
    move = current_player.get_move
    board.place_mark(move, current_player.mark)
    switch_players!
  end

  #calls play_turn each time through a loop until the game is over
  def play
  end
end
