class HumanPlayer
  attr_reader :name
  #@mark instance variable
  def initialize(name)
    @name = name
    @mark = []
  end

  #print the board out to the console
  def display(board)
    p board.grid
  end

  #allows the player to enter a move of the form '0,0' and treturn it as a position of the form [0,0]
  def get_move
    puts "where"
    input = gets.chomp
    input.split(', ').map(&:to_i)
  end
end
