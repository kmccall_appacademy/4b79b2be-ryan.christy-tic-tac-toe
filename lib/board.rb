class Board
  attr_reader :grid


  def initialize(grid=[[nil, nil, nil],[nil, nil, nil],[nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    a = pos[0]; b = pos[1]
    @grid[a][b] = mark
    # raise "" if !empty?
  end

  def empty?(pos)
    a = pos[0]; b = pos[1]
    @grid[a][b].nil?
  end

  #this one returns a mark
  def winner

      if (@grid[0][0] == @grid[1][1]) && (@grid[1][1] == @grid[2][2])
        return @grid[1][1] unless @grid[1][1].nil?
      end

      if (@grid[0][2] == @grid[1][1]) && (@grid[1][1] == @grid[2][0])
        return @grid[1][1] unless @grid[1][1].nil?
      end

      idx = 0
      while idx < 3
        if (@grid[0][idx] == @grid[1][idx]) && (@grid[1][idx] == @grid[2][idx])
          return @grid[2][idx] unless @grid[2][idx].nil?
        end
        idx += 1
      end

      @grid.each do |arr|
        if arr.uniq.length == 1
          return arr[0] unless arr[0].nil?
        end
      end


    nil
  end

  #returns true or false
  def over?

    if !@grid.flatten.all?(&:nil?) && !@grid.flatten.any?(&:nil?)
      if ((@grid.flatten.count(:O) - @grid.flatten.count(:X)).abs == 1)
        return true
      end
    end
      winner != nil
  end


end


=begin

return true if @grid[0][0] == @grid[1][1] == @grid[2][2]
return true if @grid[0][2] == @grid[1][1] == @grid[2][0]

idx = 0
while idx < 3
  return true if @grid[0][idx] == @grid[1][idx] == @grid[2][idx]
  idx += 1
end

@grid.each do |arr|
  return true if arr.uniq.length == 1
end

false
!@grid.flatten.all?(&:nil?) #&& !@grid.flatten.any?(&:nil?)
return @grid[1][1] if (@grid[0][0] == @grid[1][1] == @grid[2][2]) || (@grid[0][2] == @grid[1][1] == @grid[2][0])
=end
