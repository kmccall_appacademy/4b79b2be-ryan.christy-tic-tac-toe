class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark
  #@mark instance variable
  def initialize(name)
    @name = name
    @mark = nil
  end

  #store the board it's passed as an instance variable, so that get_move has access to it
  def display(board)
    @board = board
    p board.grid
  end

  #returns a winning move if one is available. If not one is, move randomly.

  def get_move
    #talking column
    idx2 = 0
    while idx2 < 3
      arr1 = [board.grid[0][idx2], board.grid[1][idx2], board.grid[2][idx2]]
      if arr1.count(mark) == 2 && arr1.any?(&:nil?)
        idx1 = arr1.index(nil)
        return [idx1, idx2]
      end
      idx2 += 1
    end

    #talking rows
    board.grid.each.with_index do |arr, idx|
      if (arr.count(mark) == 2) && arr.any?(&:nil?)
        idx0 = arr.index(nil)
        return [idx, idx0]
      end
    end

    arr3 = [board.grid[0][0], board.grid[1][1], board.grid[2][2]]
    if arr3.count(mark) == 2 && arr3.any?(&:nil?)
      idx3 = arr3.index(nil)
      return [idx3, idx3]
    end

    arr4 = [board.grid[0][2], board.grid[1][1], board.grid[2][0]]
    if arr4.count(mark) == 2 && arr4.any?(&:nil?)
      idx4 = arr4.index(nil)
      return [idx4, ridx(idx4)]
    end

    a = [0,1,2].shuffle.last
    b = [0,1,2].shuffle.last
    return [a,b]

  end

  private

  def ridx(idx)
    return 1 if idx == 1
    return 0 if idx == 2
    return 2 if idx == 0
  end

end

=begin

def get_move
  objective: get the winning move when available.
  move = []

  #talking column
  idx2 = 0
  while idx2 < 3
    arr1 = [board.grid[0][idx2], board.grid[1][idx2], board.grid[2][idx2]]
    if arr1.count(mark) == 2 && arr1.any?(&:nil?)
      idx1 = arr1.index(nil)
      return [idx1, idx2]
    end
  end <-- while end

  #talking rows
  board.grid.each.with_index do |arr,idx|
    if arr.count(mark) == 2 && arr.any?(&:nil?)
      idx0 = arr.index(nil)
      return [idx, idx0]
    end
  end

  arr3 = [board.grid[0][0], board.grid[1][1], board.grid[2][2]]
  if arr3.count(mark) == 2 && arr3.any?(&:nil?)
    idx3 = arr3.index(nil)
    return [idx3, idx3]
  end

  arr4 = [board.grid[0][2], board.grid[1][1], board.grid[2][0]]
  if arr4.count(mark) == 2 && arr4.any?(&:nil?)
    idx4 = arr4.index(nil)
    return [idx4, ridx(idx4)]
  end

  c = [1,2]

  until c.nil?
    a = [0,1,2].shuffle.last
    b = [0,1,2].shuffle.last
    c = board.grid[a][b]
    return [a,b] if c.nil?
  end

end

def ridx(idx)
  return 1 if idx == 1
  return 0 if idx == 2
  return 2 if idx == 0
end


return true if @grid[0][0] == @grid[1][1] == @grid[2][2]
return true if @grid[0][2] == @grid[1][1] == @grid[2][0]

idx = 0
while idx < 3
  return true if @grid[0][idx] == @grid[1][idx] == @grid[2][idx]
  idx += 1
end

@grid.each do |arr|
  return true if arr.uniq.length == 1
end


=end
